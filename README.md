# Projectname frontend application
* `git clone git@bitbucket.org:projectname/projectname.git`
* `cd projectname`
* `npm install`
* put `.env` file with contents given below
* `npm start`

### .env

    UV_THREADPOOL_SIZE=100
    SERVER_PORT=4000
    API_URL=http://api.projectname
    PROXY=true
