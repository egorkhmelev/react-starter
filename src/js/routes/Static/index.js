export default [
  {
    path: '/',
    component: require('./components/Main')
  },
  {
    path: 'test',
    component: require('./components/Test')
  }
]
