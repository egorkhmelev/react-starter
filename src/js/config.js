export default {
  port: process.env.SERVER_PORT || 4000,
  apiUrl: process.env.API_URL || 'http://api.staging.web.site',
}
